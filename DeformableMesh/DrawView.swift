//
//  DrawView.swift
//  DeformableMesh
//
//  Created by Artem Shevchenko on 02.12.2019.
//  Copyright © 2019 Lachlan Hurst. All rights reserved.
//

import UIKit

struct TargetPoV {
    var cameraPoint: CGPoint
    var targetPoints: [CGPoint]
}

struct PointAddress {
    var lineNumber: Int
    var pointNumber: Int
}

class DrawView: UIView {
    
    var drawMode: DrawMode = .borders {
        didSet {
            holdingTarget = nil
            if drawMode == .addTarget || drawMode == .target {
                targets.forEach { $0.isUserInteractionEnabled = true }
            } else {
                targets.forEach { $0.isUserInteractionEnabled = false }
            }
        }
    }
    var touch : UITouch!
    var borderArray : [[CGPoint]] = []
    var cameraArray: [CGPoint] = []
    var targetArray: [TargetPoV] = []
    var targets: [UIImageView] = []
    
    var parentVC: ViewController!
    
    var holdingTarget: UIView? {
        didSet {
            isRotating = false
        }
    }
    var isRotating: Bool = false
    var targetScale: CGFloat = 0.1
    // address of the line and point in the borderArray
    var selectedPoints: [PointAddress] = []
    // minimal distance to consider two points as the same
    let minimalDistance = UIScreen.main.bounds.width * 0.03
    
    // MARK: - Touches
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        superview?.endEditing(true)
        guard touches.count == 1 else {
            return
        }
        touch = touches.first! as UITouch
        let lastPoint = touch.location(in: self)
        switch self.drawMode {
        case .borders:
            borderArray.append([CGPoint]())
            borderArray[borderArray.count - 1].append(lastPoint)
        case .camera:
            cameraArray.append(lastPoint)
        case .target:
            if touch.view?.isKind(of: UIImageView.self) ?? false {
                let point = CGPoint(x: cameraArray[cameraArray.count - 1].x, y: cameraArray[cameraArray.count - 1].y)
                var contains = false
                for i in 0..<targetArray.count {
                    if targetArray[i].cameraPoint == point {
                        contains = true
                        targetArray[i].targetPoints.append(touch.view!.center)
                        break
                    }
                }
                if !contains {
                    let pov = TargetPoV(cameraPoint: point, targetPoints: [touch.view!.center])
                    targetArray.append(pov)
                }
            }
        case .addTarget:
            if touch.view == self {
                for each in targets {
                    let tmpPoint = each.subviews[0].convert(lastPoint, from: self)
                    if each.subviews[0].point(inside: tmpPoint, with: event) {
                        holdingTarget = each
                        isRotating = true
                        return
                    }
                }
                
                holdingTarget = nil
                let imageView = UIImageView(image: UIImage(named: "target_icon"))
                let arrow = UIImageView(image: UIImage(named: "direction"))
                imageView.addSubview(arrow)
                arrow.sizeToFit()
                arrow.center =  CGPoint(x: imageView.frame.midX, y: imageView.frame.maxY + arrow.frame.height / 2)
                arrow.isUserInteractionEnabled = true
                addSubview(imageView)
                targets.append(imageView)
                imageView.center = lastPoint
                imageView.isUserInteractionEnabled = true
                imageView.transform = CGAffineTransform.identity.scaledBy(x: targetScale, y: targetScale)
            } else {
                if touch.view?.superview == self {
                    holdingTarget = touch.view
                } else {
                    holdingTarget = touch.view?.superview
                    isRotating = true
                }
            }
        }
        setNeedsDisplay()
        self.parentVC.allowRemoving()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard touches.count == 1 else {
            return
        }
        touch = touches.first! as UITouch
        let currentPoint = touch.location(in: self)
        
        self.setNeedsDisplay()
        if let holdingTarget = self.holdingTarget {
            if isRotating == false {
                holdingTarget.center = currentPoint
            } else {
                rotate(to: currentPoint)
            }
        }
        if self.drawMode == .borders {
            // pull the selected point
            if selectedPoints.count > 0 {
                for address in selectedPoints {
                    borderArray[address.lineNumber][address.pointNumber] = currentPoint
                }
            }
            // draw the new line
            else {
                if borderArray[borderArray.count - 1].count < 2 {
                    borderArray[borderArray.count - 1].append(currentPoint)
                }
                else {
                    borderArray[borderArray.count - 1][1] = currentPoint
                }
            }
            self.setNeedsDisplay()
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        touch = touches.first! as UITouch
        let lastPoint = touch.location(in: self)
        if self.drawMode == .borders {
            // edit exist line mode
            if selectedPoints.count > 0 {
                if distance(point1: borderArray[borderArray.count - 1][0], point2: lastPoint) <= minimalDistance {
                    // select other point or deselect
                    selectedPoints = isSamePoint(lastPoint)
                    borderArray.removeLast()
                }
            }
            // create new line mode or select extist point
            else {
                if borderArray[borderArray.count - 1].count > 0 {
                    // finish new line
                    if distance(point1: borderArray[borderArray.count - 1][0], point2: lastPoint) > minimalDistance {
                        borderArray[borderArray.count - 1][1] = lastPoint
                        combineNeighborPoints()
                    }
                    // delete new line if it's length less than minimal distance
                    else {
                        selectedPoints = isSamePoint(lastPoint)
                        borderArray.removeLast()
                    }
                }
            }
            self.setNeedsDisplay()
        }
    }
    
    // MARK: - Drawing
    
    override func draw(_ rect: CGRect) {

        let scrollView = superview as? UIScrollView
        if borderArray.count > 0 || cameraArray.count > 0 || targetArray.count > 0 {
            let context = UIGraphicsGetCurrentContext()
            context!.setLineWidth(4)
            context!.setLineCap(.round)
            context!.setLineJoin(.round)
            
            for curve in borderArray {
                context!.setStrokeColor((UIColor(red:0.00, green:0.38, blue:0.83, alpha:1.0)).cgColor)
                context!.beginPath()
                context?.move(to: curve[0])
                for point in 1..<curve.count {
                    context?.addLine(to: curve[point])
                }
                context!.strokePath()
            }
            if selectedPoints.count > 0 {
                context!.setStrokeColor((UIColor(red:1.0, green:0.0, blue:0.0, alpha:1.0)).cgColor)
                context!.addArc(center: borderArray[selectedPoints[0].lineNumber][selectedPoints[0].pointNumber], radius: 2.5, startAngle: 0.0, endAngle: CGFloat.pi * 2.0, clockwise: true)
                context!.strokePath()
            }
            if cameraArray.count > 0 {
                context!.setStrokeColor((UIColor(red:1.00, green:0.0, blue:0.0, alpha:1.0)).cgColor)
                context!.beginPath()
                let width = 5 / (scrollView?.zoomScale ?? 1)
                context!.setLineWidth(width)
                context?.move(to: cameraArray[0])
                for point in 0..<cameraArray.count {
                    context?.addLine(to: cameraArray[point])
                }
                context!.strokePath()
            }
            if targetArray.count > 0 {
                context!.setLineWidth(1)
                context?.setStrokeColor((UIColor(red: 1.00, green: 0.78, blue: 0.04, alpha: 1.0)).cgColor)
                context!.beginPath()
                for camera in targetArray {
                    for target in camera.targetPoints {
                        context?.move(to: camera.cameraPoint)
                        context?.addLine(to: target)
                        context!.strokePath()
                        context!.fillEllipse(in: CGRect(x: target.x - 2.0, y: target.y - 2.0, width: 4.0, height: 4.0))
                    }
                }
            }
        }
        targetScale = 2 / (scrollView?.zoomScale ?? 1)
        if targetScale > 0.5 {
            targetScale = 0.5
        }
        for each in targets {
            let angle = atan2f(Float(each.transform.b), Float(each.transform.a))
            each.transform = CGAffineTransform.identity
            each.transform = each.transform.rotated(by: CGFloat(angle))
            each.transform = each.transform.scaledBy(x: targetScale, y: targetScale)
        }
    }
    
    // MARK: - Service methods
    
    private func rotate(to point: CGPoint) {
        let p1 = CGPoint(x: holdingTarget!.center.x, y: holdingTarget!.center.y + holdingTarget!.frame.height / 2 + holdingTarget!.subviews[0].frame.height)
        let p2 = point
        let v1 = CGVector(dx: p1.x - holdingTarget!.center.x, dy: p1.y - holdingTarget!.center.y)
        let v2 = CGVector(dx: p2.x - holdingTarget!.center.x, dy: p2.y - holdingTarget!.center.y)
        
        let angle = atan2(v2.dy, v2.dx) - atan2(v1.dy, v1.dx)
        var transform = CGAffineTransform(rotationAngle: angle)
        transform = transform.scaledBy(x: targetScale, y: targetScale)
        holdingTarget?.transform = transform
    }
    
    private func isSamePoint(_ point: CGPoint) -> [PointAddress] {
        var result: Array<PointAddress> = []
        for lineNumber in 0..<borderArray.count {
            for pointNumber in 0..<borderArray[lineNumber].count {
                if borderArray[lineNumber].count > 1 && distance(point1: borderArray[lineNumber][pointNumber], point2: point) <= minimalDistance {
                    result.append(PointAddress(lineNumber: lineNumber, pointNumber: pointNumber))
                }
            }
        }
        return result
    }
    
    private func combineNeighborPoints() {
        // checks if points of the last line locate near points of other lines
        if var line = borderArray.last {
            for lineNumber in 0..<borderArray.count - 1 {
                for pointNumber in 0..<borderArray[lineNumber].count {
                    if distance(point1: borderArray[lineNumber][pointNumber], point2: line[0]) <= minimalDistance {
                        line[0] = borderArray[lineNumber][pointNumber]
                    }
                    else if distance(point1: borderArray[lineNumber][pointNumber], point2: line[1]) <= minimalDistance {
                        line[1] = borderArray[lineNumber][pointNumber]
                    }
                }
            }
            borderArray[borderArray.count - 1] = line
        }
    }
    
    private func distance(point1: CGPoint, point2: CGPoint) -> CGFloat {
        return sqrt((point2.x - point1.x) * (point2.x - point1.x) + (point2.y - point1.y) * (point2.y - point1.y))
    }
}
