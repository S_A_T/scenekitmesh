//
//  GameViewController.swift
//  DeformableMesh
//
// Copyright (c) 2015 Lachlan Hurst
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import Metal
import UIKit
import QuartzCore
import SceneKit
import SpriteKit

class GameViewController: UIViewController, SCNSceneRendererDelegate {

    var deformData:DeformData? = nil
    var planeNode:SCNNode? = nil
    var meshData:MetalMeshData!
    var cameraNode:SCNNode? = nil
    var rearSightNode: SKSpriteNode!

    var device:MTLDevice!

    var deformer:MetalMeshDeformer!
    var isDrawed = false
    
    //plane's mesh params
    var meshSize: CGSize = .zero
    let step:Float = 0.5
    
    // camera params
    let defaultFocalLength: CGFloat = 20.0
    
    var lines : [[CGPoint]] = [[CGPoint]()]
    var cameraPoints: [CGPoint] = []
    var targetPoints: [TargetPoV] = []
    var targets: [(CGPoint, Float)] = []

    let linearVelocity: CGFloat = 10.0 // points/sec
    let angleVelocity: CGFloat = CGFloat.pi / 1.5 // rad/sec
    var currentPointNumber = 0 // the point number where the camera is now
    var currentPoV: CGPoint = .zero // the point to which the camera is currently looking at
    var isMoveToNextPoint = false
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scnView = self.view as! SCNView
        scnView.delegate = self
        scnView.showsStatistics = true
        scnView.backgroundColor = UIColor.lightGray
        scnView.autoenablesDefaultLighting = true
        scnView.isPlaying = true
        
        let scene = createScene(in: scnView)
        
        createLight(in: scene)
        
        createCamera(in: scene, sceneView: scnView)
        
        addGestures(to: scnView)
        
        drawLines()
        
        addModels(to: scene, sceneView: scnView)
    }
    
    private func createScene(in view: SCNView) -> SCNScene {
        device = MTLCreateSystemDefaultDevice()
        deformer = MetalMeshDeformer(device: device)
        
        let scene = SCNScene()
        scene.fogStartDistance = 500
        scene.fogEndDistance = 1000
        view.scene = scene
        newMesh()
        
        return scene
    }
    
    private func createLight(in scene: SCNScene) {
        let spotLight = SCNLight()
        spotLight.type = SCNLight.LightType.directional
        spotLight.color = UIColor.white
        spotLight.zNear = 100.0
        spotLight.zFar = 1000.0
        spotLight.castsShadow = true
        spotLight.shadowBias = 5
        spotLight.spotOuterAngle = 55
        spotLight.spotOuterAngle = 75
        let spotLightNode = SCNNode()
        spotLightNode.light = spotLight
        
        var spotLightTransform = SCNMatrix4Identity
        spotLightTransform = SCNMatrix4Translate(spotLightTransform, Float(meshSize.width / 2), Float(-meshSize.height / 2), 400)
        spotLightTransform = SCNMatrix4Rotate(spotLightTransform, -65 * Float.pi / 180, 1, 0, 0)
        spotLightNode.transform = spotLightTransform
        spotLightNode.name = "Light"
        
        scene.rootNode.addChildNode(spotLightNode)
        
        let ambientLight = SCNLight()
        ambientLight.color = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        ambientLight.type = SCNLight.LightType.ambient
        let ambientLightNode = SCNNode()
        ambientLightNode.light = ambientLight
        scene.rootNode.addChildNode(ambientLightNode)
    }
    
    private func createCamera(in scene: SCNScene, sceneView: SCNView) {
        if self.cameraPoints.count > 1 {
            self.cameraNode = SCNNode()
            self.cameraNode!.camera = SCNCamera()
            self.cameraNode!.camera?.zNear = 0.1
            self.cameraNode!.camera?.zFar = 500
            self.cameraNode!.camera?.focalLength = defaultFocalLength
            self.cameraNode!.position = SCNVector3(self.cameraPoints[0].x, 2, self.cameraPoints[0].y)
            self.cameraNode?.look(at: SCNVector3(self.cameraPoints[1].x, 2, self.cameraPoints[1].y))
            self.currentPoV = CGPoint(x: self.cameraPoints[1].x, y: self.cameraPoints[1].y)
            scene.rootNode.addChildNode(self.cameraNode!)
            sceneView.pointOfView = self.cameraNode!
        }
        else {
            sceneView.allowsCameraControl = true
        }
    }
    
    private func addGestures(to sceneView: SCNView) {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(GameViewController.newMesh))
        tapGesture.numberOfTapsRequired = 2
        sceneView.addGestureRecognizer(tapGesture)
    }
    
    private func drawLines() {
        guard let box = planeNode?.boundingBox else {
            return
        }
        let time = Date().timeIntervalSince1970
        for each in lines {
            for pointIndex in 0..<each.count {
                if pointIndex == each.count - 1 {
                    let vector = SCNVector3(each[pointIndex].x, CGFloat(box.max.y), each[pointIndex].y)
                    draw(at: vector)
                }
                else if calculateDistance(point1: each[pointIndex], point2: each[pointIndex + 1]) > 0 {
                    let dx = each[pointIndex + 1].x - each[pointIndex].x
                    let dy = each[pointIndex + 1].y - each[pointIndex].y
                    let steps = Int(max(abs(dx), abs(dy)) / 0.8)
                    if steps > 0 {
                        let xStep = dx / CGFloat(steps)
                        let yStep = dy / CGFloat(steps)
                        for i in 0..<steps {
                            let vector = SCNVector3(each[pointIndex].x + xStep * CGFloat(i), CGFloat(box.max.y), each[pointIndex].y + yStep * CGFloat(i))
                            draw(at: vector)
                        }
                    } else {
                        let vector = SCNVector3(each[pointIndex].x, CGFloat(box.max.y), each[pointIndex].y)
                        draw(at: vector)
                    }
                }
            }
        }
        print(Date().timeIntervalSince1970 - time)
    }
    
    private func addModels(to scene: SCNScene, sceneView: SCNView) {
        guard let myScene = SCNScene(named: "art.scnassets/scene.scn")
            else { fatalError("Unable to load scene file.") }
        
        if let node = myScene.rootNode.childNode(withName: "IPSC_Paper", recursively: true) {
            for each in targets {
                let node = node.clone()
                
                node.worldPosition = SCNVector3(each.0.x, 1, each.0.y)
                let orientation = node.orientation
                var glQuaternion = GLKQuaternionMake(orientation.x, orientation.y, orientation.z, orientation.w)
                let multiplier = GLKQuaternionMakeWithAngleAndAxis(each.1, 0, 1, 0)
                glQuaternion = GLKQuaternionMultiply(glQuaternion, multiplier)
                node.orientation = SCNQuaternion(x: glQuaternion.x, y: glQuaternion.y, z: glQuaternion.z, w: glQuaternion.w)
                scene.rootNode.addChildNode(node)
            }
            
            addSpriteScene(to: sceneView)
        }
    }
    
    private func addSpriteScene(to scene: SCNView) {
        let spriteScene = SKScene(size: self.view.bounds.size)
        rearSightNode = SKSpriteNode(imageNamed: "rearSight")
        rearSightNode.size = CGSize(width: 100, height: 100)
        rearSightNode.position = CGPoint(x: scene.bounds.size.width * 0.5, y: scene.bounds.size.height * 0.5 - 40)
        rearSightNode.isHidden = true
        spriteScene.addChild(rearSightNode)
        scene.overlaySKScene = spriteScene
    }
    
    // MARK: - Override methods
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        moveNext()
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Drawing
    
    func draw(at point: SCNVector3) {
        let loc = float3.init(arrayLiteral: point.x, point.y,  point.z)
        let localDir  = SCNVector3(0, 25, 0)

        let dir = float3.init(localDir)

        let dd = DeformData(location:loc, direction:dir, radiusSquared:4, deformationAmplitude: 0.1, pad1: 0, pad2: 0)
        self.deformData = dd
        
        guard let deformData = self.deformData else {
            return
        }
        deformer.deform(meshData, deformData: deformData)
        self.deformData = nil
    }
    
    func moveNext() {
        if let mainCamera = self.cameraNode {
            var sequenceArr: [SCNAction] = []
            
            // targeting the camera
            var targetPoV: TargetPoV?
            for pov in self.targetPoints {
                if pov.cameraPoint == self.cameraPoints[self.currentPointNumber] {
                    targetPoV = pov
                }
            }
            
            if let pov = targetPoV {
                for targetPoint in pov.targetPoints {
                    var angle = self.calculateAngle(middlePoint: self.cameraPoints[self.currentPointNumber], point1: self.currentPoV, point2: targetPoint)
                    if self.isMoveToNextPoint {
                        angle = angle > 0 ? CGFloat.pi - angle : -(angle + CGFloat.pi)
                    }
                    else {
                        angle = -angle
                    }
                    self.currentPoV = targetPoint
                    let targetingAction = SCNAction.rotate(by: angle, around: SCNVector3(0, 1, 0), duration: Double(abs(angle) / angleVelocity))
                    targetingAction.timingMode = .easeInEaseOut
                    sequenceArr.append(targetingAction)
                    let zoomInAction = SCNAction.customAction(duration: 0.4) { (_, _) in
                        self.rearSightNode.isHidden = false
                        SCNTransaction.begin()
                        SCNTransaction.animationDuration = 0.4
                        mainCamera.camera?.focalLength = self.defaultFocalLength + CGFloat(mainCamera.worldPosition.distance(SCNVector3(targetPoint.x, 2, targetPoint.y))) * 5
                        SCNTransaction.commit()
                    }
                    let zoomOutAction = SCNAction.customAction(duration: 0.4) { (_, _) in
                        SCNTransaction.begin()
                        SCNTransaction.animationDuration = 0.4
                        mainCamera.camera?.focalLength = self.defaultFocalLength
                        SCNTransaction.commit()
                        self.rearSightNode.isHidden = true
                    }
                    sequenceArr.append(zoomInAction)
                    sequenceArr.append(zoomOutAction)
                    self.isMoveToNextPoint = false
                }
            }

            // if this is not the last point, rotate and move the camera to the next one
            if currentPointNumber < self.cameraPoints.count - 1 {
                // rotate to next point action
                var angle = self.calculateAngle(middlePoint: self.cameraPoints[self.currentPointNumber], point1: self.currentPoV, point2: self.cameraPoints[self.currentPointNumber + 1])
                if self.isMoveToNextPoint {
                    angle = angle > 0 ? CGFloat.pi - angle : -(angle + CGFloat.pi)
                }
                else {
                    angle = -angle
                }
                let rotateAction = SCNAction.rotate(by: angle, around: SCNVector3(0, 1, 0), duration: Double(abs(angle) / angleVelocity))
                rotateAction.timingMode = .easeInEaseOut
                sequenceArr.append(rotateAction)
                
                // move action
                let point = SCNVector3Make(Float(self.cameraPoints[self.currentPointNumber + 1].x), 2, Float(self.cameraPoints[self.currentPointNumber + 1].y))
                let distance = calculateDistance(point1: self.cameraPoints[self.currentPointNumber], point2: self.cameraPoints[self.currentPointNumber + 1])
                let moveAction = SCNAction.move(to: point, duration: Double(distance / linearVelocity))
                sequenceArr.append(moveAction)
            }
            
            // run actions
            if sequenceArr.count > 0 {
                let sequence = SCNAction.sequence(sequenceArr)
                mainCamera.runAction(sequence) {
                    if self.currentPointNumber < self.cameraPoints.count - 1 {
                        self.currentPoV = self.cameraPoints[self.currentPointNumber]
                        self.currentPointNumber += 1
                        self.isMoveToNextPoint = true
                        self.moveNext()
                    }
                }
            }
        }
    }

    @objc func newMesh() {
        let scnView = self.view as! SCNView
        
        meshData = MetalMeshDeformable.buildPlane(device, width: Float(meshSize.width), length: Float(meshSize.height), step: step)
        let newPlaneNode = SCNNode(geometry: meshData.geometry)
        newPlaneNode.geometry?.firstMaterial?.diffuse.contents = UIImage(named: "texture")
        newPlaneNode.castsShadow = true

        if let existingNode = planeNode {
            scnView.scene?.rootNode.replaceChildNode(existingNode, with: newPlaneNode)
        } else {
            scnView.scene?.rootNode.addChildNode(newPlaneNode)
        }
        planeNode = newPlaneNode
    }
    
    // MARK: - Service methods
    
    /**
    @method calculateDistance:
    @abstract Calculates distance between two points.
    @param point1 The start point.
    @param point2 The finish point.
    */
    private func calculateDistance(point1: CGPoint, point2: CGPoint) -> CGFloat {
        let dx = abs(point2.x - point1.x)
        let dy = abs(point2.y - point1.y)
        let result = CGFloat(sqrt(dx * dx + dy * dy))
        return result
    }
    
    /**
    @method calculateAngle:
    @abstract Calculates the angle created by 3 points. Used to define on which angle the camera should rotate.
    @param middlePoint The point where the camera is currently located.
    @param point1 The point at which the camera looked before the start of the turn.
    @param point2 The point at which the camera will look after the end of the rotation.
    */
    private func calculateAngle(middlePoint: CGPoint, point1: CGPoint, point2: CGPoint) -> CGFloat {
        let vector1 = (point1.x - middlePoint.x, point1.y - middlePoint.y)
        let vector2 = (point2.x - middlePoint.x, point2.y - middlePoint.y)
        let dotProduct = vector1.0 * vector2.0 + vector1.1 * vector2.1
        let magnitude1 = sqrt(vector1.0 * vector1.0 + vector1.1 * vector1.1)
        let magnitude2 = sqrt(vector2.0 * vector2.0 + vector2.1 * vector2.1)
        let cos = dotProduct / (magnitude1 * magnitude2)
        if abs(cos - 1.0) < 0.0001 {
            return 0.0
        }
        var result = acos(cos)
        let direction = vector1.0 * vector2.1 - vector1.1 * vector2.0
        if direction < 0 {
            result = -result
        }
        return result
    }
  
}

