//
//  ViewController.swift
//  DeformableMesh
//
//  Created by Artem Shevchenko on 02.12.2019.
//  Copyright © 2019 Lachlan Hurst. All rights reserved.
//

import UIKit

enum DrawMode: Int {
    case borders = 0, camera, target, addTarget
}

class ViewController: UIViewController {

    @IBOutlet weak var widthField: UITextField!
    @IBOutlet weak var lengthField: UITextField!
    @IBOutlet weak var areaView: DrawView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var elementsSelector: UISegmentedControl!
    @IBOutlet weak var removeLastButton: UIBarButtonItem!
    
    var meshSize: CGSize!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        meshSize = areaView.frame.size
        scrollView.maximumZoomScale = 30
        scrollView.delegate = self
        scrollView.minimumZoomScale = 0.3
        for each in scrollView.gestureRecognizers ?? [] {
            if let gesture = each as? UIPanGestureRecognizer {
                gesture.minimumNumberOfTouches = 2
                gesture.maximumNumberOfTouches = 2
            }
        }
        self.removeLastButton.isEnabled = false
        self.areaView.parentVC = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        zoomCorrection()
    }
    
    @IBAction func applyTouched(_ sender: Any) {
        guard let width = Int(widthField.text ?? "") else { return }
        guard let length = Int(lengthField.text ?? "") else { return }
        view.endEditing(true)
        let widthConstraint = areaView.constraints.first { (constraint) -> Bool in
            return constraint.identifier == "width"
        }
        let heightConstraint = areaView.constraints.first { (constraint) -> Bool in
            return constraint.identifier == "height"
        }
        
        widthConstraint?.constant = CGFloat(width)
        heightConstraint?.constant = CGFloat(length)
        meshSize = CGSize(width: width, height: length)
        areaView.layoutIfNeeded()
        zoomCorrection()
    }
    
    @IBAction func modeChanged(_ sender: UISegmentedControl) {
        if let mode = DrawMode(rawValue: sender.selectedSegmentIndex) {
            self.areaView.drawMode = mode
        }
    }
    
    @IBAction func resetTouched(_ sender: UIButton) {
        if self.areaView.drawMode == .borders {
            self.areaView.borderArray.removeAll()
        } else if self.areaView.drawMode == .camera {
            self.areaView.cameraArray.removeAll()
        } else if self.areaView.drawMode == .target {
            self.areaView.targetArray.removeAll()
        } else if self.areaView.drawMode == .addTarget {
            self.areaView.targets.forEach { $0.removeFromSuperview() }
            self.areaView.targets.removeAll()
            self.areaView.holdingTarget = nil
        }
        self.areaView.setNeedsDisplay()
    }
    
    @IBAction func addTarget(_ sender: Any) {
        areaView.drawMode = .addTarget
    }
    
    @IBAction func removeLastTouch(_ sender: Any) {
        switch self.areaView.drawMode {
        case .borders:
            guard self.areaView.borderArray.count > 0 else { return }
            self.areaView.borderArray.removeLast()
        case .camera:
            guard self.areaView.cameraArray.count > 0 else { return }
            for (index, each) in self.areaView.targetArray.enumerated() {
                if each.cameraPoint == self.areaView.cameraArray.last {
                    self.areaView.targetArray.remove(at: index)
                }
            }
            self.areaView.cameraArray.removeLast()
        case .target:
            guard self.areaView.targetArray.count > 0 else { return }
            self.areaView.targetArray.removeLast()
        case .addTarget:
            guard self.areaView.targets.count > 0 else { return }
            self.areaView.targets.last?.removeFromSuperview()
            self.areaView.targets.removeLast()
        }
        self.areaView.setNeedsDisplay()
    }
    
    @IBAction func nextTouch(_ sender: Any) {
        performSegue(withIdentifier: "next", sender: nil)
    }
    
    public func allowRemoving() {
        self.removeLastButton.isEnabled = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? GameViewController {
            vc.lines = areaView.borderArray
            vc.cameraPoints = areaView.cameraArray
            vc.targetPoints = areaView.targetArray
            vc.meshSize = meshSize
            var targets: [(CGPoint, Float)] = []
            for each in areaView.targets {
                let angle = -atan2f(Float(each.transform.b), Float(each.transform.a))
                let item = (each.center, angle)
                targets.append(item)
            }
            vc.targets = targets
        }
    }
    
    func zoomCorrection() {
        let offsetX = max((scrollView.bounds.width - scrollView.contentSize.width) * 0.5, 0)
        let offsetY = max((scrollView.bounds.height - scrollView.contentSize.height) * 0.5, 0)
        scrollView.contentInset = UIEdgeInsetsMake(offsetY, offsetX, 0, 0)
    }

}

extension ViewController: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return areaView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        zoomCorrection()
        view.endEditing(true)
        areaView.setNeedsDisplay()
    }
    
}
